package com.btrco.example.sfid;

import com.btrco.example.sfid.aspect.Countable;
import com.btrco.example.sfid.config.ApplicationConfig;
import com.btrco.example.sfid.service.GreetingService;
import com.btrco.example.sfid.service.OutputService;
import com.btrco.example.sfid.service.TimeService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    @Countable
    public static void main(String[] args) throws Exception {
       ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
       OutputService outputService = context.getBean(OutputService.class);

        for (int i=0;i<5;i++){
            outputService.generateOutput();
            Thread.sleep(1000);
        }
    }
}
